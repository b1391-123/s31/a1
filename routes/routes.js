const express = require("express");
const controller = require("../controllers/controllers");
const router = express.Router();

router.get("/", (req, res) => {
  controller.getAllUsers().then((result) => {
    res.send(result);
  });
});

router.post("/add-user", (req, res) => {
  controller.register(req.body).then((result) => {
    res.send(result);
  });
});

router.put("/update-user", (req, res) => {
  controller.updateUser(req.body.email).then((result) => {
    res.send(result);
  });
});

router.delete("/delete-user", (req, res) => {
  controller.deleteUser(req.body.email).then((result) => {
    res.send(result);
  });
});

router.get("/specific-user", (req, res) => {
  controller.getSpecificUser(req.body.email).then((result) => {
    res.send(result);
  });
});

router.get("/:id", (req, res) => {
  controller.getById(req.params.id).then((result) => {
    res.send(result);
  });
});
module.exports = router;
