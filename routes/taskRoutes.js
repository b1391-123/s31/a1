const express = require("express");
const router = express.Router();
const taskController = require("../controllers/taskController");

router.post("/", (req, res) => {
  taskController.createTask(req.body).then((result) => {
    res.send(result);
  });
});

router.get("/get-task", (req, res) => {
  taskController.getSpecificTask(req.body.name).then((result) => {
    res.send(result);
  });
});

router.put("/update-task", (req, res) => {
  taskController.updateTask(req.body.name).then((result) => {
    res.send(result);
  });
});

router.get("/:id", (req, res) => {
  taskController.getById(req.params.id).then((result) => {
    res.send(result);
  });
});

router.get("/get-all", (req, res) => {
  taskController.getAllTasks().then((result) => {
    res.send(result);
  });
});

module.exports = router;
