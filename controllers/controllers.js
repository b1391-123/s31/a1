const User = require("../models/Users");

module.exports.getAllUsers = () => {
  return User.find().then((result) => {
    return result;
  });
};

module.exports.register = (reqBody) => {
  return User.findOne({ email: reqBody.email }).then((result) => {
    if (result != null) {
      return "User already exist";
    } else {
      let newUser = new User({
        firstName: reqBody.firstName,
        lastName: reqBody.lastName,
        email: reqBody.email,
        password: reqBody.password,
      });
      return newUser.save().then((result) => {
        return "user saved!";
      });
    }
  });
};

module.exports.updateUser = (email) => {
  let updatedAdminStatus = {
    isAdmin: true,
  };
  return User.findOneAndUpdate({ email: email }, updatedAdminStatus, {
    new: true,
  }).then((result) => {
    return result;
  });
};

module.exports.deleteUser = (email) => {
  return User.findOneAndDelete({ email: email }).then((result) => {
    return true;
  });
};

module.exports.getSpecificUser = (email) => {
  return User.findOne({ email: email }).then((result) => {
    return result;
  });
};

module.exports.getById = (id) => {
  return User.findById(id).then((result) => {
    return result;
  });
};
