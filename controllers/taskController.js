const Task = require("../models/Task");

module.exports.createTask = (reqBody) => {
  let newTask = new Task({
    name: reqBody.name,
  });
  return newTask.save().then((result) => result);
};

module.exports.getSpecificTask = (name) => {
  return Task.findOne({ name: name }).then((result) => {
    return result;
  });
};

module.exports.updateTask = (name) => {
  let updatedStatus = {
    status: "complete",
  };
  return Task.findOneAndUpdate({ name: name }, updatedStatus, {
    new: true,
  }).then((result) => {
    return result;
  });
};

module.exports.getById = (id) => {
  return Task.findById(id).then((result) => {
    return result;
  });
};
