const express = require("express");
const port = process.env.port || 4000;
const app = express();
const mongoose = require("mongoose");
const routes = require("./routes/routes");
const taskRoutes = require("./routes/taskRoutes");

mongoose.connect("mongodb+srv://aeroyabes2:aero123@batch139.3ldu1.mongodb.net/myFirstDatabase?retryWrites=true&w=majority", {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

const db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error:"));
db.once("open", () => console.log("Connected to database"));

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use("/api/tasks", taskRoutes);
app.use("/api/users", routes);

app.listen(port, () => console.log(`connected to localhost:${port}`));
